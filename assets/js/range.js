//Range Input
$(function () {

    // Initiate Slider
    $('#slider-range').slider({
        range: true,
        min: 18,
        max: 55,
        step: 1,
        values: [18, 35]
    });
    // Move the range wrapper into the generated divs
    $('.ui-slider-range').append($('.range-wrapper'));

    $('.mobile-value .range-one').html('<span class="range-value">' + $('#slider-range').slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span>');
    $('.mobile-value .range-two').html('<span class="range-value">'
        + $("#slider-range").slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
        + '</span>');

    // Apply initial values to the range container
    $('.range-wrapper.one .range').html('<span class="range-value">' + $('#slider-range').slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span>');
    $('.range-wrapper.two .range').html('<span class="range-value">'
        + $("#slider-range").slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
        + '</span>');

    $('#slider-range').slider({
        slide: function (event, ui) {

            $('.mobile-value .range-one').html('<span class="range-value">' + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '' +
                '</span>');
            $('.mobile-value .range-two').html('<span class="range-value">'
                + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                + '</span>');
            // Update the range container values upon sliding

            $('.range-wrapper.one .range').html('<span class="range-value">' + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '' +
                '</span>');
            $('.range-wrapper.two .range').html('<span class="range-value">'
                + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                + '</span>');
            // Get old value
            var previousVal = parseInt($(this).data('value'));

            // Save new value
            $(this).data({
                'value': parseInt(ui.value)
            });
        }
    });
    // Prevent the range container from moving the slider
    $('.range, .range-alert').on('mousedown', function (event) {
        event.stopPropagation();
    });

    let touchElement = document.getElementsByClassName('ui-slider')[0];
    touchElement.addEventListener("touchmove", touchHandler, false);
    touchElement.addEventListener("touchend", touchCancel, false);

    function touchHandler(event)
    {
        /*if (event.type === 'touchstart' && event.cancelable) event.preventDefault();*/
        if (event.cancelable) {
            event.preventDefault();
        }

        $('.mobile-value').css('opacity','1')
    }
    function touchCancel(event)
    {
        /* if (event.type === 'touchend' && event.cancelable) event.preventDefault();*/
        if (event.cancelable) {
            event.preventDefault();
        }
        $('.mobile-value').css('opacity','0')

    }
});
//Range Input end---------
