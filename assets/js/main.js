import './general';

/*Şifre değişrit kısmında üstüne geldiğinde buton renginin değişmesi*/
$('.general-btn.bg-purple').hover(
    function() {
        if ($(this).hasClass('active')){
            $( this ).attr('style','background-color: #7288ff !important');
        }
    }, function() {
        if ($(this).hasClass('active')){
            $( this ).attr('style','background-color: #6478fd !important');
        }
    }
)

/*Blog Swiper*/
var swiper = new Swiper(".blogSwiper", {
    loop:true,
    autoplay:{
        delay: 3000,
        disableOnInteraction: false,
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
});

